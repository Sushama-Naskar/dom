# DOM
The Document Object Model(DOM) describes the document structure. It represents the content of HTML or XML document as nodes in a hierarchical tree structure, where the `Document` interface represents the entire HTML or XML document. The `Document` interface is the root of the document tree, it provides access to the document's data. Each node in the tree represents a single object(such as a text or an element) within a document.

In an HTML document, the `<html>` is the root node, which provides access to all other nodes and objects of a document.


```html
<html>
    <head>
        <title>This is a title</title>
    </head>
    <body>
        
        <h1>This is a heading</h1>
        <p>This is a paragraph</p>
        
    </body>
</html>

```

![tree representation](dom.png)

# HOW DOES IT HELP
 The DOM is a programming interface or API, so we can use DOM to read, access, or update the content of documents. 

 Every web browser uses the DOM object model to make the web page accessible for programs. When a web page is loaded in the browser, the browser creates a DOM of the webpage. Programming languages can interact with the web page and change the document structure, style, and content with the help of DOM. DOM doesn't depend on any programming language.
 
 


# HOW TO ACCESS IT

We can simply use the API for the document or window object to manipulate or access the document.

## But first let us understand some common types of node:

|  |  |
---|---
Root Node | provides access to all other nodes and objects of a document. In an HTML document, the `<html>` is the root node.
Element Node| contains the HTML tags.
Attribute Node | contains information about an element node.
Parent Node | has another node inside it.
Child Node |this is a node that is directly inside another node.
sibling Node | nodes with the same parent node.
Text Node | contains a text or a string.

DOM specifies the different interface to handle different types of nodes, it has a nodelist interface to handle an ordered list of nodes, such as the children of a node, and also an interface to handle unordered sets of nodes with the reference of their name attribute(if we make any changes to a node in the tree, the changes will reflect in all references to the node in nodelist objects).


Accessing the content of an HTML document using JavaScript:

```html
<html>
    <head>
        <title>This is a title</title>
    </head>
    <body>
        
        <h1>This is a heading</h1>
        <p> id="para">This is a paragraph</p>
        
    </body>
     <script>
        window.onload=function(){
        document.getElementById('para').innerHTML="Changes the content";
        }
    </script>
</html>

```
Accessing the content of an XML file in python:
```python
import xml.dom.minidom

doc=xml.dom.minidom.parse("myxmlfile.xml");
para=doc.getElementsByTagName("p");
```

# WHAT ARE JS DOM HELPER METHOD

JS dom is an object that provides access to all HTML elements of a document. We can access and update the content, style, or the document's structure using JS DOM helper methods.

## Some DOM helper methods:

|  |  |
---|---
`getElementById()` | returns the first element with the given id value.     
`getElementsByTagName()` | returns all elements with the given tag name.        
`getElementsByClassName()` | returns all the elements with the given class name. 
`querySelector()` | returns the first element that matches the specified CSS selector.
`querySelectorAll()` | returns all the elements that match a specified CSS selector.
`innerHTML` | gets and sets the HTML content of a node.
`createElement()` | creates a new element.
`appendChild()` | appends a node to a child list of a parent node.
`append()` | adds a node after the last child node of the selected parent node.
`removeChild()` | removes the child element of a node.
`addEventListener();` | calls a function when a specified event happens.
`setAttribute()` | sets the value of an attribute.
`getArrribute()` | gets the value of an attribute of an element.
`removeAttribute()` | removes an attribute from a specific element.


# REFERENCES
[MDN Web Docs](https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model/Introduction)


[Tools](https://www.toolsqa.com/javascript/dom-in-javascript/)
